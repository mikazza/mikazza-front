FROM node:lts-alpine AS build
ARG REACT_APP_BASE_URL
ENV REACT_APP_BASE_URL=$REACT_APP_BASE_URL

WORKDIR /app
COPY /src .
RUN yarn install
RUN yarn run build

# stage 2
FROM nginx:1.20.1-alpine
LABEL maintainer="tle@tlnk.fr"

WORKDIR /usr/share/nginx/html/

# Clean the default public folder
RUN rm -fr * .??*

# This inserts a line in the default config file, including our file "expires.inc"
RUN sed -i '9i\        include /etc/nginx/conf.d/expires.inc;\n' /etc/nginx/conf.d/default.conf

# The file "expires.inc" is copied into the image
COPY /nginx/expires.inc /etc/nginx/conf.d/expires.inc
COPY /nginx/default.conf /etc/nginx/conf.d/default.conf
RUN chmod 0644 /etc/nginx/conf.d/expires.inc

COPY --from=build /app/build/ /usr/share/nginx/html

ENV PORT=80
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'


LABEL org.label-schema.name="tlnk.fr"
LABEL org.label-schema.description="mikazza-front"
LABEL org.label-schema.url="https://mikazza.io"
LABEL org.label-schema.vendor="TLNK"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vcs-url="https://gitlab.com/mikazza/mikazza-front"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.docker.cmd="docker run registry.gitlab.com/mikazza/mikazza-front"
