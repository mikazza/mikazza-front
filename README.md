# Mikazza Front

backend api his hosted on heroku

## Requirements

* python3
* npm
* nodejs
  
## Setup virtual environment

Source: [https://nbdime.readthedocs.io/en/latest/nodevenv.html](https://nbdime.readthedocs.io/en/latest/nodevenv.html)

``` cmd
python3 -m venv .venv
pip install nodeenv
nodeenv -p
npm install -g yarn
yarn install -g create-react-appnpm start
```

## Project setup

``` cmd
yarn install
```

### Compiles and hot-reloads for development

```cmd
yarn start
```

### Compiles and minifies for production

```cmd
yarn run build
```

### Lints and fixes files

```cmd
yarn run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Docker compose

```cmd
docker-compose -f ./docker-compose.yaml build
docker-compose -f ./docker-compose.yaml up
```

### Default port and url

front: localhost:8080
api: localhost:5000
api documentation: localhost:5000/api/v1/doc
phpmyadmin: localhost:8082
mysql: 3306

## MySql privilege

If you need to grant more privilege to the default user, use the following mysql command:

```sql
GRANT ALL PRIVILEGES ON `pm`.* TO 'pm'@'%' WITH GRANT OPTION;
```
