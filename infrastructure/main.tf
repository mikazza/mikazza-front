/* -------------------------------------------------------------------------- */
/*                     Terraform state with gitlab backend                    */
/* -------------------------------------------------------------------------- */

terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5

  }
}

/* -------------------------------------------------------------------------- */
/*                                     AWS                                    */
/* -------------------------------------------------------------------------- */

locals {
  heroku = {
    region = "eu"
  }
  mikazza = {
    name               = "mikazza-front"
    domain             = "mikazza.io"
    azure_zone_rg_name = "TF_PROD_DNS_PUBLIC"

    prod = {
      heroku_formation_size = "free"
      dns_record_name       = "app"


    }

    stage = {
      heroku_formation_size = "free"
      dns_record_name       = "app.stage"
    }
  }
}

/* -------------------------------------------------------------------------- */
/*                                   Heroku                                   */
/* -------------------------------------------------------------------------- */

# resource "heroku_app" "mikazza_app_api" {
#   name   = "${local.mikazza.name}-${local.environment}"
#   region = local.heroku.region

# }

# resource "heroku_app_config_association" "mikazza_app_api" {
#   app_id = heroku_app.mikazza_app_api.id
#   sensitive_vars = {
#     ENV = local.environment
#     REACT_APP_BASE_URL = "https://mikazza-api-${local.environment}.herokuapp.com/api/v1"
#   }
# }

# resource "heroku_formation" "mikazza_formation" {
#   count    = local.mikazza[local.environment].heroku_formation_size == "free" ? 0 : 1
#   app      = heroku_app.mikazza_app_api.name
#   type     = "web"
#   size     = local.mikazza[local.environment].heroku_formation_size
#   quantity = 1
# }


# resource "heroku_domain" "mikazza_custom_domain" {
#   app      = heroku_app.mikazza_app_api.name
#   hostname = "${local.mikazza[local.environment].dns_record_name}.${local.mikazza.domain}"
# }

/* -------------------------------------------------------------------------- */
/*                                     DNS                                    */
/* -------------------------------------------------------------------------- */

resource "azurerm_dns_cname_record" "mikazza_app_record" {
  name                = local.mikazza[local.environment].dns_record_name
  zone_name           = local.mikazza.domain
  resource_group_name = local.mikazza.azure_zone_rg_name
  ttl                 = 300
  record              = "cname.vercel-dns.com"
}