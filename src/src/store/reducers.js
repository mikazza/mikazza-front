import { combineReducers } from '@reduxjs/toolkit'
import propertyReducer from '../store/propertySlice'
import userReducer from '../store/userSlice'

const reducer = combineReducers({
    property: propertyReducer,
    user: userReducer
})

export default reducer