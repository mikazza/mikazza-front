import { createSlice } from '@reduxjs/toolkit'
import api from './middlewares/api'

import { saveAs } from 'file-saver'

const propertySlice = createSlice({
    name: 'property',
    initialState: {
        properties: [],
        property: null,
        files: []
      },
    reducers: {
        getSuccess: (state, action) =>  {
          state.properties = action.payload.sort((a,b) => new Date(b.creationDate) - new Date(a.creationDate))
        },
        getOneSuccess: (state, action) =>  {
          state.property = action.payload
        },
        createSuccess: (state, action) => {
          state.properties.push(action.payload)
        },
        getFileSuccess: (state, action) =>  {
          state.files = action.payload.sort((a,b) => new Date(b.creationDate) - new Date(a.creationDate))
        }
    }
})

export default propertySlice.reducer

// Actions
const { getSuccess, getOneSuccess, getFileSuccess, createSuccess } = propertySlice.actions

export const getAllProperties = () => async dispatch => {
  try {
    const res = await api.get('/property/object', {})
    dispatch(getSuccess(res.data))
  } catch (e) {
    return console.error(e.message)
  }
}

export const getProperty = (id) => async dispatch => {
  try {
    const res = await api.get(`/property/object/${id}`)
    dispatch(getOneSuccess(res.data))
  } catch (e) {
    return console.error(e.message)
  }
}

export const create = (property) => async dispatch => {
  try {
    const res = await api.post('/property/object', property)
    dispatch(createSuccess({...property, id: res.data.id}))
  } catch (e) {
    return console.error(e.message)
  }
}

export const edit = (id, property) => async dispatch => {
  try {
    await api.put(`/property/object/${id}`, property)
    dispatch(getOneSuccess(property))
  } catch (e) {
    return console.error(e.message)
  }
}

export const getDocuments = (propertyId) => async dispatch => {
  try {
    const res = await api.get(`/property/${propertyId}/document`)
    dispatch(getFileSuccess(res.data))
  } catch (e) {
    return console.error(e.message)
  }
}

export const deleteFile = (propertyId, documentId) => async dispatch => {
  try {
    await api.delete(`/property/${propertyId}/document/${documentId}`)
  } catch (e) {
    return console.error(e.message)
  }
}

export const downloadFile = (propertyId, documentId) => async dispatch => {
  try {
    const res = await api.get(`/property/${propertyId}/document/download/${documentId}`)
    saveAs(res.data.s3_url)
      // return res.data.s3_url

    // const url = res.data.s3_url

    // const response = await axios({
    //   url,
    //   method: 'GET',
    //   responseType: 'arraybuffer',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     'Accept': '*/*' // add other types
    //   }
    // })

    // console.log(response.data)
    // return response.data
  } catch (e) {
    return console.error(e.message)
  }
}

export const postDocument = (id, files) => async dispatch => {
  try {
    files.map( async (file) => { 
      const formData = new FormData()
      formData.append('file', file) 
      const res = await api.post(`/property/${id}/document`, formData)
      const response = await api.get(`/property/${id}/document`)  // update documents list for each document
      dispatch(getFileSuccess(response.data)) 
      return file
    })
  } catch (e) {
    return console.error(e.message)
  }
}