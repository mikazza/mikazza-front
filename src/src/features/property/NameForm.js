import React from 'react'
import { useFormik } from 'formik';
import * as yup from 'yup';
import { TextField, Grid } from '@material-ui/core';

export function NameForm(props) {

  const { name } = props.formData
  const { errors,
    touched,
    handleSubmit,
    handleChange,
    isValid,
    setFieldTouched } = props.formik
  const { formData, setForm } = props

console.log(props)
  // cda: remove formik initialisation inside form steps, use props instead

 const change = (name, e) => { // cda to finish, to get a name validation to submit the form (disabled buttons when necessary)
  // e.persist();
  // // handleChange(e);
  // console.log(e, name)
  // setFieldTouched(name, true, false);
  setForm({...formData, [e.target.name]: e.target.value})
};

  return (
    <Grid item xs={12}>
      <TextField
               fullWidth
                variant="standard"
                margin="normal"
                required
                id="name"
                label="Property name"
                name="name"
                autoFocus
                value={name}
                onChange={e => change(name, e)}
                error={touched.name && Boolean(errors.name)}
                helperText={touched.name ? errors.name : ''}
      />
    </Grid>
     
  )
}