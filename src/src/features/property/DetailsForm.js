import React from 'react'
import { useFormik } from 'formik';
import * as yup from 'yup';
import { TextField, Grid, FormControlLabel, Switch } from '@material-ui/core';
import { Fragment } from 'react';


export function DetailsForm(props) {

  const { formData, setForm } = props
  const { totalSurface, livingSurface, numberOfRoom, numberOfBathroom, numberOfBalcony, numberOfBedroom, numberOfTerrasse, numberOfToilette, gardenAvailable, garageAvailable, cellarAvailable } = props.formData

  const formik = useFormik({
    initialValues: { totalSurface, livingSurface, numberOfRoom, numberOfBathroom, numberOfBalcony, numberOfBedroom, numberOfTerrasse, numberOfToilette, gardenAvailable, garageAvailable, cellarAvailable },
    enableReinitialize: true
  })

  return (
    <Fragment>
  <Grid item xs={6}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                type="number"
                id="totalSurface"
                label="Total Surface (m2)"
                name="totalSurface"
                value={formik.values.totalSurface}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                type="number"
                id="livingSurface"
                label="Living Surface (m2)"
                name="livingSurface"
                value={formik.values.livingSurface}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                type="number"
                id="numberOfRoom"
                label="Number of Rooms"
                name="numberOfRoom"
                value={formik.values.numberOfRoom}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="numberOfBathroom"
                type="number"
                label="Number of Bathrooms"
                name="numberOfBathroom"
                value={formik.values.numberOfBathroom}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="numberOfBedroom"
                type="number"
                label="Number of Bedrooms"
                name="numberOfBedroom"
                value={formik.values.numberOfBedroom}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="numberOfToilette"
                type="number"
                label="Number of Toilets"
                name="numberOfToilette"
                value={formik.values.numberOfToilette}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="numberOfBalcony"
                type="number"
                label="Number of Balcony"
                name="numberOfBalcony"
                value={formik.values.numberOfBalcony}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
              <TextField
              fullWidth
                variant="standard"
                margin="normal"
                id="numberOfTerrasse"
                type="number"
                label="Number of Terrasse"
                name="numberOfTerrasse"
                value={formik.values.numberOfTerrasse}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
            />
            </Grid>
            <Grid item xs={6}></Grid>
             <FormControlLabel
              control={<Switch 
                checked={Boolean(formik.values.garageAvailable)} 
                onChange={e => setForm({...formData, [e.target.name]: Boolean(e.target.value)})} 
                name="garageAvailable" />}
                label="Garage"
            />
             <FormControlLabel
              control={<Switch 
                checked={Boolean(formik.values.cellarAvailable)} 
                onChange={e => setForm({...formData, [e.target.name]: Boolean(e.target.value)})} 
                name="cellarAvailable" />}
                id="cellarAvailable"
                label="Cellar"
            />
            <FormControlLabel
              control={<Switch 
                checked={Boolean(formik.values.gardenAvailable)} 
                onChange={e => setForm({...formData, [e.target.name]: Boolean(e.target.value)})} 
                name="gardenAvailable" />}
                label="Garden"
                margin="normal"
                id="gardenAvailable"
            />
            <Grid item xs={6}></Grid>
    </Fragment>
  )
}