import React, { useEffect } from 'react';
import { useSelector, useDispatch} from 'react-redux';
import { useHistory } from "react-router-dom";
import { getAllProperties } from "../../store/propertySlice";
import { PropertyListItem } from "./PropertyListItem";
import { makeStyles, Container, Avatar, Grid, Typography, Fab, IconButton, List, ListItem, ListItemText, ListItemAvatar, ListItemSecondaryAction } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: '#fafafa',//theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export function PropertyList() {
  const dispatch = useDispatch();
  const properties = useSelector(state => state.property.properties)
  const history = useHistory();
  const classes = useStyles()


useEffect(() => {
  if (properties.length === 0) {
    dispatch(getAllProperties())
  }
},  [dispatch, properties.length])

const createProperty = () => {
  history.push(`/property/create`)
}

const renderProperties = properties.map( property => (
  <PropertyListItem key={property.id} {...property}/>
))

const emptyList = (<div> You don't have properties yet. </div>)

  return (
    <div className='App'>
    <Container component="main" maxWidth="md">
      <Grid container spacing={2}>
        <Grid item xs>
            <Typography variant="h4" className={classes.title}>
              My properties
            </Typography>
            <div className={classes.demo}>
              <List dense={true}>
                {renderProperties.length > 0 ? renderProperties : emptyList}
              </List>
            </div>
          </Grid>
        </Grid>
        <Fab color="secondary" aria-label="edit" style={{position: 'fixed', bottom: '30px', right: '30px', zIndex: 2}} onClick={createProperty}>
          <AddIcon />
        </Fab>
      </Container>
    </div>
  )
}