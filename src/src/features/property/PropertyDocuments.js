import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch} from 'react-redux';
import { useHistory, useParams } from "react-router-dom";
import { getProperty, postDocument, downloadFile, getDocuments, deleteFile } from "../../store/propertySlice";
import { DropzoneDialog } from 'material-ui-dropzone';
import { Container, Fab, List, ListItem, IconButton, ListItemSecondaryAction, ListItemAvatar, Avatar, ListItemText } from '@material-ui/core';
import GetAppRoundedIcon from '@material-ui/icons/GetAppRounded';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import PublishRoundedIcon from '@material-ui/icons/PublishRounded';

export function PropertyDocuments() {
  const property = useSelector(state => state.property.property)
  const files = useSelector(state => state.property.files)
  const { id } = useParams()
  const dispatch = useDispatch();
  const history = useHistory();

  const [open, setOpen] = useState(false);

useEffect(() => {
  if (property == null) {
    dispatch(getProperty(id))
    dispatch(getDocuments(id))
  }
},  [dispatch, property, id])

const handleChange = (files) => {
  dispatch(postDocument(id, files))
}

const download = (id, fileId) => {
  dispatch(downloadFile(id, fileId))
}

const handleDelete = async (id, fileId) => {
 await dispatch(deleteFile(id, fileId))
 await dispatch(getDocuments(id))
}

const renderFiles = files.map(file => (
  <ListItem key={file.id} style={{cursor: 'pointer'}}>
      <ListItemAvatar onClick={e => download(id, file.id)}>
        <Avatar>
          <GetAppRoundedIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={file.name.replace(`${file.uuid}_`, '')}
        secondary={file.creationDate}
        key={file.id}
        onClick={e => download(id, file.id)}
      />
      <ListItemSecondaryAction>
        <IconButton edge="end" aria-label="delete" onClick={e => handleDelete(id, file.id)}>
          <DeleteRoundedIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
))

  return (
    <div style={{ marginLeft: '240px'}}>
       {property && (
      <div className='App'>
        <Container component="main" maxWidth="md">
          <Fab color="secondary" aria-label="upload" style={{position: 'fixed', bottom: '30px', right: '30px', zIndex: 2}} onClick={() => setOpen(true)}>
            <PublishRoundedIcon />
          </Fab>
        
          <div>{property.name}</div>
          <div>{property.city}</div>
          <DropzoneDialog
            cancelButtonText={"cancel"}
            submitButtonText={"submit"}
            open={open}
            onClose={() => setOpen(false)}
            onSave={(files) => {
              handleChange(files);
              setOpen(false);
            }}
            showPreviews={true}
            showFileNamesInPreview={true}
          />
           <div>
              <List dense={true}>
                {renderFiles}
              </List>
            </div>
        </Container>
      </div>
       )}
    </div>
  )
}