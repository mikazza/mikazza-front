import React, { Fragment } from 'react'
import { NameForm } from "./NameForm";
import { AddressForm } from "./AddressForm";
import { DetailsForm } from "./DetailsForm";
import { CostForm } from './CostForm';

export function MultiStepForm(props) {
 const { step } = props
  switch (step) {
      case 1:
        return (
          <Fragment>
            <NameForm {...props} />
          </Fragment>);
      case 2:
        return (
          <Fragment>
            <AddressForm {...props} />
          </Fragment>);
      case 3:
        return (
          <Fragment>
            <DetailsForm {...props} />
          </Fragment>);
      case 4:
          return (
            <Fragment>
              <CostForm {...props} />
            </Fragment>);
      default:
        return null;
  }  
}