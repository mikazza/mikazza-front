import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch} from 'react-redux';
import { useHistory, useParams } from "react-router-dom";
import { getProperty } from "../../store/propertySlice";
import { makeStyles, Container, Fab, Grid, Typography, Checkbox, FormControlLabel } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: '#fafafa',//theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export function PropertyItem() {
  const property = useSelector(state => state.property.property)
  const { id } = useParams()
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles()

useEffect(() => {
  if (property == null) {
    dispatch(getProperty(id))
  }
},  [dispatch, property, id])

function edit(event, id) {
  event.preventDefault();
  history.push(`/property/${id}/edit`)
}

  return (
    <div style={{ marginLeft: '240px'}}>
       {property && (
      <div className='App'>     
        <Container component="main" maxWidth="md">
          <Grid container spacing={2}>
            <Grid item xs>
              <Typography variant="h4" className={classes.title}>
                {property.name}
              </Typography>
              <Typography variant="h6" className={classes.title} style={{ fontSize: '0.8rem', fontWeight: 600}}>
                ADDRESS
              </Typography>
              <div style={{ display: 'flex', flexDirection: 'column'}}>
                <Typography variant="p">
                  {property.address1}, {property.streetNumber}
                </Typography>
                <Typography variant="p">
                  {property.addressExtra}
                </Typography>
                <Typography variant="p">
                  {property.postalCode} {property.city}, {property.country}
                </Typography>
                <Typography variant="p">
                  Floor: {property.floor} 
                   Door: {property.door}
                </Typography>
              </div>
              <Typography variant="h6" className={classes.title} style={{ fontSize: '0.8rem', fontWeight: 600}}>
                SURFACE
              </Typography>
              <div style={{ display: 'flex', flexDirection: 'column'}}>
                <Typography variant="p">
                  Total surface : {property.totalSurface} m2
                </Typography>
                <Typography variant="p">
                  Living surface (Carrez) : {property.LivingSurface} m2
                </Typography>
              </div>
              <Typography variant="h6" className={classes.title} style={{ fontSize: '0.8rem', fontWeight: 600}}>
                ROOM DISTRIBUTION
              </Typography>
              <div style={{ display: 'flex', flexDirection: 'column'}}>
                <Typography variant="p">
                  Rooms : {property.numberOfRoom}
                </Typography>
                <Typography variant="p">
                  Bedrooms : {property.numberOfBedroom}
                </Typography>
                <Typography variant="p">
                  BathRooms : {property.numberOfBathroom}
                </Typography>
                <Typography variant="p">
                  Toilets : {property.numberOfToilette}
                </Typography>
                <Typography variant="p">
                  Terrasse : {property.numberOfTerrasse}
                </Typography>
                <Typography variant="p">
                  Balcony : {property.numberOfBalcony}
                </Typography>
                <FormControlLabel
                  control={
                  <Checkbox
                    checked={property.garageAvailable}
                    name="Garage"
                    color="primary"
                  />
                  }
                  label="Garage"
                />
                <FormControlLabel
                  control={
                  <Checkbox
                    checked={property.gardenAvailable}
                    name="Garden"
                    color="primary"
                  />
                  }
                  label="Garden"
                />
                <FormControlLabel
                  control={
                  <Checkbox
                    checked={property.cellarAvailable}
                    name="Cellar"
                    color="primary"
                  />
                  }
                  label="Cellar"
                />
              </div>
            </Grid>
          </Grid>
          <Fab color="secondary" aria-label="edit" style={{position: 'fixed', bottom: '30px', right: '30px', zIndex: 2}} onClick={e => edit(e, property.id)}>
            <EditIcon />
          </Fab>
        </Container>
      </div>
       )}
    </div>
  )
}