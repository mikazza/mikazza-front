import React from 'react'
import { useFormik } from 'formik';
import * as yup from 'yup';
import { TextField, Grid } from '@material-ui/core';
import { Fragment } from 'react';


export function CostForm(props) {
  const { formData, setForm } = props
  const { buyingCost, buyingDate, availableDate, sellingDate, source } = props.formData

  const formik = useFormik({
    initialValues: { buyingCost, buyingDate, availableDate, sellingDate, source },
    enableReinitialize: true
  })

  return (
    <Fragment>
      <Grid item xs={6}>
        <TextField
                fullWidth
                variant="standard"
                margin="normal"
                type="number"
                id="buyingCost"
                label="Buying Cost (€)"
                name="buyingCost"
                value={formik.values.buyingCost}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
        />
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={6}>
        <TextField
              fullWidth
                type="date"
                margin="normal"
                format="MM/dd/yyyy"
                id="buyingDate"
                label="Buying Date"
                name="buyingDate"
                value={formik.values.buyingDate}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
                InputLabelProps={{
                  shrink: true,
                }}
        />
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={6}>
        <TextField
              fullWidth
                type="date"
                margin="normal"
                format="MM/dd/yyyy"
                id="availableDate"
                label="Available Date"
                name="availableDate"
                value={formik.values.availableDate}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
                InputLabelProps={{
                  shrink: true,
                }}
        />
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={6}>
        <TextField
              fullWidth
                type="date"
                margin="normal"
                format="MM/dd/yyyy"
                id="sellingDate"
                label="Selling Date"
                name="sellingDate"
                value={formik.values.sellingDate}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
                InputLabelProps={{
                  shrink: true,
                }}
        />
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={6}>
        <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="source"
                label="Source"
                name="source"
                value={formik.values.source}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
        />
      </Grid>
      <Grid item xs={6}></Grid>
    </Fragment>
  )
}