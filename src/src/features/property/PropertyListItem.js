import React from 'react';
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import image from '../../assets/appart-test.jpg';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: '10px'
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 500,
  },
  image: {
    width: 128,
    height: 128,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
}));

export function PropertyListItem(props) {
  const classes = useStyles();
  const history = useHistory();
  console.log(props)
  const { id, name, creationDate, address1 } = props

  function handleSelect(e, id) {
    e.preventDefault();
    history.push(`/property/${id}/details`)
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper} >
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className={classes.image} onClick={e => handleSelect(e, id)} >
            <img className={classes.img} alt="flat-image" src={image} />

            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs style={{ cursor: 'pointer' }} onClick={e => handleSelect(e, id)}>
                <Typography gutterBottom variant="subtitle1">
                  { name ? name : 'No title' }
                </Typography>
                <Typography variant="body2" gutterBottom>
                  { creationDate }
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  Address: { address1 ? address1 : 'No address filled'}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="body2" style={{ cursor: 'pointer' }}>
                  Remove
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <Typography variant="subtitle1"></Typography>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}
