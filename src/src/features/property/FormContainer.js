import React, { useEffect, useState, Fragment } from 'react'
import { useSelector, useDispatch} from 'react-redux'
import { useHistory, useParams } from "react-router-dom";
import { getProperty } from "../../store/propertySlice";
import { create, edit } from "../../store/propertySlice";
import { Breadcrumbs, Button, Typography, Container, CssBaseline, Link, makeStyles, Grid } from '@material-ui/core';
import { MultiStepForm } from './MultiStepForm';
import { useFormik } from 'formik';
import * as yup from 'yup';

const useStyles = makeStyles((theme) => ({
  button: {
    marginLeft: '5px',
    background: theme.palette.secondary.main,
    color: '#fff'
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  step: {
    display: 'flex', 
    margin: '1%', 
    color: theme.palette.secondary.main
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    background: theme.palette.primary.main,
    color: '#fff',
    display: 'flex'
  }
}))

export function FormContainer() {
  const history = useHistory();
  let { id } = useParams()
  const classes = useStyles()
  const dispatch = useDispatch()
  const property = useSelector(state => state.property.property)
  
  useEffect(() => {
    if (!property) {
      dispatch(getProperty(id))
    }
  },  [dispatch, property, id])
 
  const initialValues = {
    name: property && property.name ? property.name : '',
    address1: property && property.address1 ? property.address1 : '',
    addressExtra: property && property.addressExtra ? property.addressExtra : '',
    postalCode: property && property.postalCode ? property.postalCode : '',
    city: property && property.city ? property.city : '',
    streetNumber: property && property.streetNumber ? property.streetNumber : '',
    floorNumber: property && property.floorNumber ? property.floorNumber : 0,
    doorNumber: property && property.doorNumber ? property.doorNumber : '',
    country: property && property.country ? property.country : '',
    totalSurface: property && property.totalSurface ? property.totalSurface : 0,
    livingSurface: property && property.livingSurface ? property.livingSurface : 0,
    numberOfRoom: property && property.numberOfRoom ? property.numberOfRoom : 0,
    numberOfBathroom: property && property.numberOfBathroom ? property.numberOfBathroom : 0,
    numberOfBedroom: property && property.numberOfBedroom ? property.numberOfBedroom : 0,
    numberOfToilette: property && property.numberOfToilette ? property.numberOfToilette : 0,
    buyingCost: property && property.buyingCost ? property.buyingCost : 0,
    garageAvailable: property && property.garageAvailable ? property.garageAvailable : false,
    cellarAvailable: property && property.cellarAvailable ? property.cellarAvailable : false,
    numberOfBalcony: property && property.numberOfBalcony ? property.numberOfBalcony : 0,
    numberOfTerrasse: property && property.numberOfTerrasse ? property.numberOfTerrasse : 0,
    gardenAvailable: property && property.gardenAvailable ? property.gardenAvailable : false,
    buyingDate: property && property.buyingDate ? property.buyingDate : '1988-04-22',
    availableDate: property && property.availableDate ? property.availableDate : '1988-04-22',
    sellingDate: property && property.sellingDate ? property.sellingDate : '1988-04-22',
    source: property && property.source ? property.source : ''
}

const nameValidationSchema = yup.object().shape({ // cda make validation schema works, is Formik even needed ?
  name: yup
    .string()
    .min(1, 'Name should have 1 character minimum')
    .required('The property name is required')
});

const formik = useFormik({
  initialValues,
  validationSchema: nameValidationSchema,
  enableReinitialize: true,
  onSubmit: () => {
    handleSubmit(formData)
  }
})

const [formData, setForm] = useState(initialValues)
const [step, setStep] = useState(1);
const props = { formData, step, setForm, formik }
console.log('formdata', formData, formik)

  const nextStep = () => {
    if (step < 4) {
      setStep(prevState => prevState + 1)
    }
  }
  const previousStep = () => {
    if (step > 1) {
      setStep(prevState => prevState - 1)
    }
  }

  const StepButtons = () => ( // Move buttons to their own components and submit data
    <div style={{display: 'flex', margin: '5% 0 2% 0'}}>
          <Button type="button" 
              variant="contained"
              classes={{root: classes.button}}
              disabled={step === 1}
              onClick={previousStep}>previous
        </Button>  
          <Button type="button" 
              variant="contained"
              classes={{root: classes.button}}
              disabled={!formData.name || step === 4}
              onClick={nextStep}>next
        </Button> 
    </div>
  );

  const SubmitButton = () => (
    <Fragment>
      <div style={{display: 'flex', margin: '2% 1%', justifyContent: 'flex-end'}}>
        <Button type="submit"
                variant="contained"
                disabled={!formData.name}
                className={classes.submit}>
                Submit property
        </Button>
    </div>
  </Fragment>
  );

  async function handleSubmit(values) {
    try {
      console.log('submit', values)
      await property && values.name ? dispatch(edit(id, values)) : dispatch(create(values))
      id ? history.push(`/property/${id}`) : history.push(`/home`)
    } catch(error) {
      console.log(error)
    }
  }

  async function handleClick() {
    try {
     history.push(`/home`)
    } catch(error) {
      console.log(error)
    }
  }

  return ( 
    <div className='App' style={{ marginLeft: '240px'}}>
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <Breadcrumbs aria-label="breadcrumb"  style={{ marginBottom: '30px'}}>
        <Link color="inherit" href="/" onClick={handleClick}>
          My Properties
        </Link>
        <Typography color="textPrimary"> { id ? 'Edit property' : 'Create property'} </Typography>
      </Breadcrumbs>
      <Typography variant="caption" color="textSecondary"> { id ? '' : 'To successfully create a property, you need to fill in the Property name.'} </Typography>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <Grid container justify="center" alignItems="center" spacing={3}>
          <MultiStepForm {...props}/>
        </Grid>
        <StepButtons />
        <div className={classes.step}> 
          Step: {step}/4
        </div>
        <SubmitButton />
      </form>
    </Container>
  </div>)
}