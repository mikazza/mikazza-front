import React from 'react'
import { useFormik } from 'formik';
import * as yup from 'yup';
import { TextField, Grid } from '@material-ui/core';
import { Fragment } from 'react';


export function AddressForm(props) {

  const { formData, setForm } = props
  const { address1, addressExtra, postalCode, city, streetNumber, floorNumber, doorNumber, country } = props.formData

  const formik = useFormik({
    initialValues: { address1, addressExtra, postalCode, city, streetNumber, floorNumber, doorNumber, country },
    enableReinitialize: true
  })

  return (
    <Fragment>
      <Grid item xs={9}>
              <TextField
              fullWidth
                variant="standard"
                margin="normal"
                id="address1"
                label="Street"
                name="address1"
                value={formik.values.address1}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
            />
            </Grid>
            <Grid item xs={3}>
              <TextField
              fullWidth
                variant="standard"
                margin="normal"
                id="streetNumber"
                label="Street Number"
                name="streetNumber"
                value={formik.values.streetNumber}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
              fullWidth
                variant="standard"
                margin="normal"
                id="floorNumber"
                type="number"
                label="Floor Number"
                name="floorNumber"
                value={formik.values.floorNumber}
                onChange={e => setForm({...formData, [e.target.name]: Number(e.target.value)})}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
              fullWidth
                variant="standard"
                margin="normal"
                id="doorNumber"
                label="Door Number"
                name="doorNumber"
                value={formik.values.doorNumber}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
              fullWidth
                variant="standard"
                margin="normal"
                id="addressExtra"
                label="Address complement"
                name="addressExtra"
                value={formik.values.addressExtra}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
            />
            </Grid>
            <Grid item xs={2}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="postalCode"
                label="Postal Code"
                name="postalCode"
                value={formik.values.postalCode}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="city"
                label="City"
                name="city"
                value={formik.values.city}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
            />
            </Grid>
            <Grid item xs={5}>
              <TextField
                fullWidth
                variant="standard"
                margin="normal"
                id="country"
                label="Country"
                name="country"
                value={formik.values.country}
                onChange={e => setForm({...formData, [e.target.name]: e.target.value})}
            />
            </Grid>
    </Fragment>
  )
}