import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../store/userSlice'
import { fade, makeStyles, Avatar, AppBar, Typography, IconButton, Toolbar, Menu, MenuItem, InputBase, ListItemSecondaryAction } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  appBar: {
    backgroundColor: '#fafafa',
    boxShadow: 'none'
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  userAvatar: {
    backgroundColor: theme.palette.secondary.main,
    color: '#fff',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.grey[500], 0.25),
    '&:hover': {
      backgroundColor: fade(theme.palette.grey[500], 0.15),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.grey[500]
  },
  inputRoot: {
    color: theme.palette.grey[700]
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  }
}));

export function Navbar() {
  const dispatch = useDispatch();
  const classes = useStyles()

  const user = useSelector(state => state.user.user)
  const userInitial = user ? user.slice(0,1).toUpperCase() : ''
  
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = () => {
    dispatch(logout())
  }

  return (
   <AppBar position="static" className={classes.appBar}>
   <Toolbar>
     <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
       {/* <MenuIcon /> */}
     </IconButton>
     <Typography variant="h6" className={classes.title}></Typography>
     <div className={classes.search}>
     <div className={classes.searchIcon}>
        <SearchIcon />
          </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
      
     <div>
      <Avatar className={classes.userAvatar} onClick={e => handleMenu(e)}>{userInitial}</Avatar>
      <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          keepMounted
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={open}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>My account</MenuItem>
          <MenuItem onClick={handleClick}>Logout</MenuItem>
        </Menu>
       </div>
   </Toolbar>
 </AppBar>
  )
}