import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export default function PrivateRoute({ component: Component, ...rest }) {
  const isLoggedIn = localStorage.getItem('token')

  return (
    <Route
      { ...rest }
      render={(props) => isLoggedIn !== null ? <Component {...props} /> : <Redirect to='/' />} 
    />
  );
}