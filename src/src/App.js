import React from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import { Register } from './features/auth/Register';
import { Login } from './features/auth/Login';
import { Home } from './features/Home';
import { Navbar } from './features/Navbar';
import { ClippedDrawer } from './features/ClippedDrawer';
import { PropertyItem } from './features/property/PropertyItem';
import { PropertyDocuments } from './features/property/PropertyDocuments';
import { PropertyContacts } from './features/property/PropertyContacts';
import { FormContainer } from './features/property/FormContainer';
import PrivateRoute from './features/PrivateRoute';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import './App.scss';

const theme = createMuiTheme({
  palette: {
    primary: {main: '#10d48e'},
    secondary: {main: '#0ca56e'}
  }
});

function App() {
  const isLoggedIn = useSelector(state => state.user.token) || localStorage.getItem('token')
    return (
      <Router>
        <MuiThemeProvider theme={theme}>
        <div className="App">
          {isLoggedIn !== null ? <Navbar /> : null}
          {isLoggedIn !== null ? <ClippedDrawer /> : null}
          <Switch>
            <Route exact path="/" render={() => ( isLoggedIn ? <Redirect to='/home' /> : <Login />)} />
            <Route path="/register" render={() => (<Register />)} />
            <Route path="/login" render={() => (<Login />)} />
            <PrivateRoute exact path="/home" component={Home} />
            <PrivateRoute exact path="/property/create" component={FormContainer} />
            <PrivateRoute exact path="/property/:id/details" component={PropertyItem} />
            <PrivateRoute exact path="/property/:id/documents" component={PropertyDocuments} />
            <PrivateRoute exact path="/property/:id/contacts" component={PropertyContacts} />
            <PrivateRoute exact path="/property/:id/edit" component={FormContainer} />           
          </Switch>
        </div>
        </MuiThemeProvider>
      </Router>
    );
}

export default App;
